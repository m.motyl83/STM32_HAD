#include "config.h"
#include "debug.h"
#include "libeth/enc28j60.h"
#include "global_defines.h"
#include "hadRequests.h"
#include "libeth/ethernet.h"
#include "libeth/spi.h"
#include "misc.h"
#include "state_machine.h"

static sfsTask netTask;
static spi_func_t stm32spi_func;

//TODO: add send/receive timeouts
static uint8_t spi_sendrecv(uint8_t byte)
{
    uint8_t rxData;

    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
        ;
    SPI_I2S_SendData(SPI1, byte);

    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)
        ;
    rxData = SPI_I2S_ReceiveData(SPI1);

    // Wait for end of transmission
    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
        ;

    return rxData;
}

static void stm32_spi_select(bool Selected)
{
    GPIO_WriteBit(ENC28J68_CS_PORT, ENC28J68_CS_PIN, !Selected);
}

static void stm32_spi_transfer_write(uint8_t Data)
{
    spi_sendrecv(Data);
}

static uint8_t stm32_spi_transfer_read(void) { return spi_sendrecv(0xff); }

/**
 * @brief
 *
 */
static void stm32_enc28j60_hw_reset()
{
    // HW reset
    GPIO_WriteBit(ENC28J68_RESET_PORT, ENC28J68_RESET_PIN, 0);
    delayMs(50);
    GPIO_WriteBit(ENC28J68_RESET_PORT, ENC28J68_RESET_PIN, 1);
}

static uint8_t stm32spiInit()
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA |
                               RCC_APB2Periph_SPI1,
                           ENABLE);

    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Pin = SPI1_SCK_PIN | SPI1_MOSI_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(SPI1_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Pin = SPI1_MISO_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(SPI1_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Pin = ENC28J68_CS_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(ENC28J68_CS_PORT, &GPIO_InitStruct);

    GPIO_WriteBit(ENC28J68_CS_PORT, ENC28J68_CS_PIN, 1);

    SPI_InitTypeDef SPI_InitStructure;
    SPI_StructInit(&SPI_InitStructure);

    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_Init(SPI1, &SPI_InitStructure);

    SPI_Cmd(SPI1, ENABLE);

    return 0;
}

void EXTI1_IRQHandler(void)
{
#ifdef ETH_USE_INTERRUPTS
    ethernet_data_interrupt();
    netTask.hasInterrupt = true;
#endif
    EXTI_ClearITPendingBit(EXTI_Line1);
}

/**
 * @brief SysLedInit
 * @return
 */
static bool netInit()
{
    GPIO_InitTypeDef GpioInitstructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA,
                           ENABLE);

    GPIO_StructInit(&GpioInitstructure);
#ifdef ETH_USE_INTERRUPTS
    //PA1
    GpioInitstructure.GPIO_Mode = GPIO_Mode_IPU;
    GpioInitstructure.GPIO_Pin = GPIO_Pin_1;
    GpioInitstructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GpioInitstructure);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);

    EXTI_InitTypeDef EXTI_InitStructure;
    EXTI_InitStructure.EXTI_Line = EXTI_Line1;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitTypeDef NVIC_InitStructure;
    /* Enable and set EXTI0 Interrupt to the lowest priority */
    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

    GpioInitstructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GpioInitstructure.GPIO_Pin = ENC28J68_RESET_PIN;
    GpioInitstructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(ENC28J68_RESET_PORT, &GpioInitstructure);

    if (libeth_spi_init(&stm32spi_func) == 0)
    {
        // HW reset
        stm32_enc28j60_hw_reset();

        //move to libeth?
        enc28j60_initialise(getConfigData()->mac, true);

        DEBUG_MSG("ENC29J60 HW REV.:0x%x\n", enc28j60_get_revision_id());

        struct sysConfig *conf = getConfigData();
        ethernet_initialise(conf->staticIp, conf->netmask, conf->gateway);

        tcp_open_port(DEFAULT_PORT, 1000, &HadTCPOpen, &HadTCPClose, &HadTCPCb);
        udp_open_port(DEFAULT_PORT, 1000, &HadUdpOpen, &HadUdpClose, &HadUdpCb);
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

static void netFunction(sfsTask *tdata)
{
    static uint8_t oneSecCnt = 100;

    if (tdata->hasInterrupt == true)
    {
        tdata->hasInterrupt = false;
        tdata->timeToRun_ms = 10;
    }
    else
    {
        if (!oneSecCnt--)
        {
            oneSecCnt = 100;
            ethernet_second_tick();
        }
    }

    ethernet_update();
}

void initNetworkTask() { addTask(&netTask); }

static sfsTask netTask = {
    .id = 0xff,
    .timeToRun_ms = 10,
    .lastResult = 0,
    .hasInterrupt = false,
    .exec = netFunction,
    .init = netInit,
    .check_condition = NULL,
    .private_data = NULL,
    .exit = NULL,
    .suspendTask = false,
    .init_failed = false,
};

static spi_func_t stm32spi_func = {
    .spi_init_f = stm32spiInit,
    .spi_read_f = stm32_spi_transfer_read,
    .spi_write_f = stm32_spi_transfer_write,
    .spi_select_f = stm32_spi_select,
    .spi_hw_reset = stm32_enc28j60_hw_reset,
};