.syntax	unified
.arch armv7-m
.fpu softvfp

.text
.thumb
.align 2

.global ds18b20_reset_asm
.type	ds18b20_reset_asm, %function

.equ DQ_PIN_OFFSET, 4
.equ DQ_PORT_OFFSET, 0
.equ CYCCNT, 0xE0001004
.equ DELAY_480us, 0x8700
.equ DELAY_420us, 0x7620
.equ DELAY_60us, 0x10e0

ds18b20_reset_asm:
    push {r4 - r7, lr}
    sub sp, 16
    add r7, sp, 0

    # gpio ODR
    ldr r1, [r0, DQ_PORT_OFFSET]
    orr r1, 12
    str r1, [r7 , 4]

    # gpio IDR
    ldr r1, [r0, DQ_PORT_OFFSET]
    orr r1, 8
    str r1, [r7, 8]

    # clear GPIO
    ldr r2, [r7, 4]
    ldr r2, [r2]

    ldr r3, [r0, DQ_PIN_OFFSET]

    bic r2, r2, r3
    ldr r1, [r7, 4]
    str r2, [r1]

    ldr r3, =CYCCNT
    movw r1, DELAY_480us
    movw r6, 0
    str r6, [r3]
delay480us:
    ldr  r6, [r3]
    subs r6, r1
    blt delay480us

    # set GPIO
    ldr r2, [r7, 4]
    ldr r2, [r2]

    ldr r3, [r0, DQ_PIN_OFFSET]
    orr r2, r3
    ldr r1, [r7, 4]
    str r2, [r1]

    ldr r3, =CYCCNT
    movw r6, 0
    str r6, [r3]
    movw r1, DELAY_60us
delay60us:
    ldr  r6, [r3]
    subs r6, r1
    blt delay60us

    mov r1,r0
    mov r0, 0
    #return value gpio bit in r0
    ldr r4, [r7, 8]
    ldr r4, [r4]
    ldr r2, [r1, DQ_PIN_OFFSET]
    ands r4, r4, r2
    it ne
    movne r0, 1

    movw r6, 0
    ldr r3, =CYCCNT
    str r6, [r3]
    movw r1, DELAY_420us
delay420us:
    ldr  r6, [r3]
    subs r6, r1
    blt delay420us

    add r7, 16
    mov sp, r7
    pop {r4 -r7, pc}


.global ds18b20_writebit_asm
.type	ds18b20_writebit_asm, %function
ds18b20_writebit_asm:
    push {r4 - r7, lr}

    sub sp, 16
    add r7, sp, 0

    #bit to write
    str r1, [r7]

    # gpio ODR
    ldr r1, [r0, DQ_PORT_OFFSET]
    orr r1, 12
    str r1, [r7 , 4]

    # gpio IDR
    ldr r1, [r0, DQ_PORT_OFFSET]
    orr r1, 8
    str r1, [r7, 12]

    # clear GPIO
    ldr r2, [r7, 4]
    ldr r2, [r2]

    ldr r3, [r0, DQ_PIN_OFFSET]

    bic r2, r2, r3
    ldr r1, [r7, 4]
    str r2, [r1]

    ldr r3, =CYCCNT
    movw r1, 72
    movw r6, 0
    str r6, [r3]
delay1us:
    ldr  r6, [r3]
    subs r6, r1
    blt delay1us

    ldr r1, [r7]
    ands r1,r1, 1
    it ne
    bne fff
    # set GPIO
    ldr r2, [r7, 4]
    ldr r2, [r2]

    ldr r3, [r0, DQ_PIN_OFFSET]
    orr r2, r3
    ldr r1, [r7, 4]
    str r2, [r1]

fff:
    ldr r3, =CYCCNT
    movw r6, 0
    str r6, [r3]
    movw r1, DELAY_60us
delay60us2:
    ldr  r6, [r3]
    subs r6, r1
    blt delay60us2

    # set GPIO
    ldr r2, [r7, 4]
    ldr r2, [r2]

    ldr r3, [r0, DQ_PIN_OFFSET]
    orr r2, r3
    ldr r1, [r7, 4]
    str r2, [r1]

    add r7, 16
    mov sp, r7
    pop {r4 -r7, pc}



.global ds18b20_readbit_asm
.type	ds18b20_readbit_asm, %function
ds18b20_readbit_asm:
    push {r7, lr}
    mov r7, sp

    mov sp, r7
    pop {r7, pc}

.end

