/*! \file output.c \brief Output configuration and manipulation. */
//*****************************************************************************
//
// File Name            : 'output.c'
// Title		: Output configuration and manipulation
// Author		: Michal Motyl
// Version		: 0.1
// Target MCU           : Atmel AVR series
//
//*****************************************************************************
#include "output.h"
#include "config.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "system.h"
#include "debug.h"

#include "drivers/stm32f10x_conf.h"
#include "drivers/stm32f10x_gpio.h"
#include "stm32f10x_it.h"

#include <stdbool.h>
#include <stdio.h>

static output_node relayNodes[OUTPUT_MAX_NODES] = {
    {
        .port = GPIOB,
        .pin = GPIO_Pin_12,
        .enabled = 0,
    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_13,
        .enabled = 0,
    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_14,
        .enabled = 0,
    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_15,
        .enabled = 0,
    }};

/**
 * @brief initOutputDevice
 */
void initOutputDevice()
{
    GPIO_InitTypeDef GpioInitstructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    struct sysConfig *cfg = getConfigData();

    if (cfg == NULL)
        return;

    for (int i = 0, j = 0; i < cfg->feturesCnt; i++)
    {
        struct devFeatures *dev_feature = &getFeatureData()[i];

        if (dev_feature->featureId == 0xff)
            continue;

        if (dev_feature->featureType != REALY_OUTPUT)
            continue;

        relayNodes[j].enabled = true;
        relayNodes[j].featureId = dev_feature->featureId;

        GpioInitstructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GpioInitstructure.GPIO_Speed = GPIO_Speed_50MHz;
        GpioInitstructure.GPIO_Pin = relayNodes[j].pin;

        GPIO_Init(relayNodes[j].port, &GpioInitstructure);
        GPIO_PinLockConfig(relayNodes[j].port, relayNodes[j].pin);
        GPIO_WriteBit(relayNodes[j].port, relayNodes[j].pin, Bit_RESET);
        j++;
    }
}

/**
 * @brief getOutputStatus
 * @param idx
 * @return
 */
uint8_t getOutputStatus(outputNodesNames outId)
{
    if (relayNodes[outId].enabled == false)
        return 0;

    return GPIO_ReadOutputDataBit(relayNodes[outId].port, relayNodes[outId].pin);
}

/**
 * @brief getOutputStatusByFid
 * @param featureId
 * @return
 */
uint8_t getOutputStatusByFid(uint8_t featureId)
{
    for (uint8_t i = 0; i < OUTPUT_MAX_NODES; i++)
    {
        if (relayNodes[i].featureId == featureId && relayNodes[i].enabled)
            return GPIO_ReadOutputDataBit(relayNodes[i].port, relayNodes[i].pin);
    }
    return 0;
}

/**
 * @brief setOutputStatus
 * @param val
 * @param idx
 */
void setOutputStatus(outputNodesNames outId, FunctionalState new_state)
{
    if (relayNodes[outId].enabled == false)
        return;

    GPIO_WriteBit(relayNodes[outId].port, relayNodes[outId].pin, (bool)new_state);
}

void setOutputStatusByFid(uint8_t featureId, FunctionalState new_state)
{
    for (uint8_t i = 0; i < OUTPUT_MAX_NODES; i++)
    {
        if (relayNodes[i].featureId == featureId && relayNodes[i].enabled)
        {
            GPIO_WriteBit(relayNodes[i].port, relayNodes[i].pin, (BitAction)new_state);
            return;
        }
    }
}
