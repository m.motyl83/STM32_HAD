/**
 ******************************************************************************
 * @file    misc.c
 * @author  MCD Application Team
 * @version V3.5.0
 * @date    11-March-2011
 * @brief   This file provides all the miscellaneous firmware functions (add-on
 *          to CMSIS functions).
 ******************************************************************************
 * @attention
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "misc.h"
#include "drivers/stm32f10x_conf.h"
#include "drivers/stm32f10x_it.h"

/** @addtogroup STM32F10x_StdPeriph_Driver
 * @{
 */

/** @defgroup MISC
 * @brief MISC driver modules
 * @{
 */

/** @defgroup MISC_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup MISC_Private_Defines
 * @{
 */

#define AIRCR_VECTKEY_MASK ((uint32_t)0x05FA0000)
/**
 * @}
 */

/** @defgroup MISC_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup MISC_Private_Variables
 * @{
 */

/**
 * @}
 */

/** @defgroup MISC_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup MISC_Private_Functions
 * @{
 */

/**
 * @brief  Configures the priority grouping: pre-emption priority and
 * subpriority.
 * @param  NVIC_PriorityGroup: specifies the priority grouping bits length.
 *   This parameter can be one of the following values:
 *     @arg NVIC_PriorityGroup_0: 0 bits for pre-emption priority
 *                                4 bits for subpriority
 *     @arg NVIC_PriorityGroup_1: 1 bits for pre-emption priority
 *                                3 bits for subpriority
 *     @arg NVIC_PriorityGroup_2: 2 bits for pre-emption priority
 *                                2 bits for subpriority
 *     @arg NVIC_PriorityGroup_3: 3 bits for pre-emption priority
 *                                1 bits for subpriority
 *     @arg NVIC_PriorityGroup_4: 4 bits for pre-emption priority
 *                                0 bits for subpriority
 * @retval None
 */
void NVIC_PriorityGroupConfig(uint32_t NVIC_PriorityGroup) {
  /* Check the parameters */
  assert_param(IS_NVIC_PRIORITY_GROUP(NVIC_PriorityGroup));

  /* Set the PRIGROUP[10:8] bits according to NVIC_PriorityGroup value */
  SCB->AIRCR = AIRCR_VECTKEY_MASK | NVIC_PriorityGroup;
}

/**
 * @brief  Initializes the NVIC peripheral according to the specified
 *         parameters in the NVIC_InitStruct.
 * @param  NVIC_InitStruct: pointer to a NVIC_InitTypeDef structure that
 * contains
 *         the configuration information for the specified NVIC peripheral.
 * @retval None
 */
void NVIC_Init(NVIC_InitTypeDef *NVIC_InitStruct) {
  uint32_t tmppriority = 0x00, tmppre = 0x00, tmpsub = 0x0F;

  /* Check the parameters */
  assert_param(IS_FUNCTIONAL_STATE(NVIC_InitStruct->NVIC_IRQChannelCmd));
  assert_param(IS_NVIC_PREEMPTION_PRIORITY(
      NVIC_InitStruct->NVIC_IRQChannelPreemptionPriority));
  assert_param(
      IS_NVIC_SUB_PRIORITY(NVIC_InitStruct->NVIC_IRQChannelSubPriority));

  if (NVIC_InitStruct->NVIC_IRQChannelCmd != DISABLE) {
    /* Compute the Corresponding IRQ Priority --------------------------------*/
    tmppriority = (0x700 - ((SCB->AIRCR) & (uint32_t)0x700)) >> 0x08;
    tmppre = (0x4 - tmppriority);
    tmpsub = tmpsub >> tmppriority;

    tmppriority = (uint32_t)NVIC_InitStruct->NVIC_IRQChannelPreemptionPriority
                  << tmppre;
    tmppriority |= NVIC_InitStruct->NVIC_IRQChannelSubPriority & tmpsub;
    tmppriority = tmppriority << 0x04;

    NVIC->IP[NVIC_InitStruct->NVIC_IRQChannel] = tmppriority;

    /* Enable the Selected IRQ Channels --------------------------------------*/
    NVIC->ISER[NVIC_InitStruct->NVIC_IRQChannel >> 0x05] =
        (uint32_t)0x01 << (NVIC_InitStruct->NVIC_IRQChannel & (uint8_t)0x1F);
  } else {
    /* Disable the Selected IRQ Channels -------------------------------------*/
    NVIC->ICER[NVIC_InitStruct->NVIC_IRQChannel >> 0x05] =
        (uint32_t)0x01 << (NVIC_InitStruct->NVIC_IRQChannel & (uint8_t)0x1F);
  }
}

/**
 * @brief  Sets the vector table location and Offset.
 * @param  NVIC_VectTab: specifies if the vector table is in RAM or FLASH
 * memory.
 *   This parameter can be one of the following values:
 *     @arg NVIC_VectTab_RAM
 *     @arg NVIC_VectTab_FLASH
 * @param  Offset: Vector Table base offset field. This value must be a
 * multiple
 *         of 0x200.
 * @retval None
 */
void NVIC_SetVectorTable(uint32_t NVIC_VectTab, uint32_t Offset) {
  /* Check the parameters */
  assert_param(IS_NVIC_VECTTAB(NVIC_VectTab));
  assert_param(IS_NVIC_OFFSET(Offset));

  SCB->VTOR = NVIC_VectTab | (Offset & (uint32_t)0x1FFFFF80);
}

/**
 * @brief  Selects the condition for the system to enter low power mode.
 * @param  LowPowerMode: Specifies the new mode for the system to enter low
 * power mode.
 *   This parameter can be one of the following values:
 *     @arg NVIC_LP_SEVONPEND
 *     @arg NVIC_LP_SLEEPDEEP
 *     @arg NVIC_LP_SLEEPONEXIT
 * @param  NewState: new state of LP condition. This parameter can be: ENABLE
 * or DISABLE.
 * @retval None
 */
void NVIC_SystemLPConfig(uint8_t LowPowerMode, FunctionalState NewState) {
  /* Check the parameters */
  assert_param(IS_NVIC_LP(LowPowerMode));
  assert_param(IS_FUNCTIONAL_STATE(NewState));

  if (NewState != DISABLE) {
    SCB->SCR |= LowPowerMode;
  } else {
    SCB->SCR &= (uint32_t)(~(uint32_t)LowPowerMode);
  }
}

/**
 * @brief  Configures the SysTick clock source.
 * @param  SysTick_CLKSource: specifies the SysTick clock source.
 *   This parameter can be one of the following values:
 *     @arg SysTick_CLKSource_HCLK_Div8: AHB clock divided by 8 selected as
 * SysTick clock source.
 *     @arg SysTick_CLKSource_HCLK: AHB clock selected as SysTick clock source.
 * @retval None
 */
void SystickClksourceconfig(uint32_t SysTick_CLKSource) {
  /* Check the parameters */
  assert_param(IS_SYSTICK_CLK_SOURCE(SysTick_CLKSource));
  if (SysTick_CLKSource == SysTick_CLKSource_HCLK) {
    SysTick->CTRL |= SysTick_CLKSource_HCLK;
  } else {
    SysTick->CTRL &= SysTick_CLKSource_HCLK_Div8;
  }
}

/**
 * @brief systickInit
 * @param frequency
 */
void systickInit(uint16_t frequency) {
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);
  (void)SysTick_Config(RCC_Clocks.HCLK_Frequency / frequency);
}

/**
 * @brief delayMs
 * @param t
 */
void delayMs(uint32_t ms) {
  uint32_t end;
  end = getSystemTickMs() + ms;

  while (getSystemTickMs() < end)
    ;
}

/**
 * @brief initUsDelay
 */
void initUsDelay() {
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  // Time base configuration
  TIM_TimeBaseInitTypeDef TIM;
  TIM_TimeBaseStructInit(&TIM);
  TIM.TIM_Prescaler = (72000000UL / 1000000UL) - 1;
  TIM.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM2, &TIM);

  TIM_Cmd(TIM2, ENABLE);
}

#if 1
/**
 * @brief delayUs
 * @param uSecs
 */
void delayUs(uint16_t uSecs) {
  TIM2->CNT = 0;
  while ((TIM2->CNT) < uSecs)
    ;
}

void initUsDelayDwt() {
  DEMCR_MM |= DEMCR_TRCENA;
  DWT_LAR = 0xC5ACCE55;
  *DWT_CYCCNT = 0;
  /* Enable CPU cycle counter */
  DWT_CTRL |= CYCCNTENA;
}

uint32_t getDwtCycles(){
    return *DWT_CYCCNT;
}

void delayUsDwt(uint32_t uSecs) {
    uint32_t targetTick = *DWT_CYCCNT + uSecs * (72000000/1000000);
      while (*DWT_CYCCNT <= targetTick);
}

#else
void delayUs(uint16_t uSecs) {
  uint16_t cnt = TIM2->CNT;
  uint16_t cnt2 = cnt + uSecs;

  if (cnt2 < 0xffff) {
    while ((TIM2->CNT) < cnt2)
      ;
  } else {
    if (cnt2 < cnt) {
      while (TIM2->CNT)
        ;
      while (TIM2->CNT < cnt2)
        ;
    }
  }
}
#endif
/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
