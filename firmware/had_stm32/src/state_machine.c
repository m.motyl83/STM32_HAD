#include "state_machine.h"
#include "debug.h"
#include "drivers/stm32f10x_it.h"

/**
 * @brief TaskTable - keeps pointers to tasks structures
 */
static sfsTask *TaskTable[TASK_TABLE_SIZE] = {NULL};
static sfsTask *currentTask = NULL;
static volatile bool int_req = false;
static int intTaskId = -1;

void signalINT(int taskID){
    int_req = true;
    intTaskId = taskID;
}
/**
 * @brief addTask
 * @param td
 */
int addTask(sfsTask *td) {
    static uint8_t task_id_cnt = 0;
    if (td == NULL)
        return ERROR;

    if (td->exec == NULL)
        return ERROR;

    for (int i = 0; i < TASK_TABLE_SIZE; i++) {
        if (TaskTable[i] == NULL) {
            TaskTable[i] = td;
            if (td->init) {
                if (td->init() != SUCCESS) {
                    td->init_failed = 1;
                }
            }
            td->id = task_id_cnt;
            task_id_cnt++;
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
 * @brief removeTask
 * @param td
 */
int removeTask(sfsTask *td) {
    for (int i = 0; i < TASK_TABLE_SIZE; i++) {
        if (TaskTable[i] == td) {
            if (td->exit) {
                td->exit();
            }
            TaskTable[i] = NULL;
            return SUCCESS;
        }
    }
    return ERROR;
}

/**
 * @brief handleTasks
 */
void handleTasks() {
    static uint32_t currentTick;
    bool updateTimedTasks = false;

    // update tasks with timeToRun only when new tick
    if (currentTick != getSystemTickMs()) {
        currentTick = getSystemTickMs();
        updateTimedTasks = true;
    }

    for (int i = 0; i < TASK_TABLE_SIZE; i++) {

        if(int_req){
            int_req = false;
            i = intTaskId;

            currentTask = TaskTable[i];
        }else
            currentTask = TaskTable[i];

        if (currentTask == NULL)
            continue;

        if (currentTask->init_failed || currentTask->suspendTask)
            continue;

        if (currentTask->timeToRun_ms) {
            if (updateTimedTasks || currentTask->hasInterrupt) {

                if (currentTask->hasInterrupt) {
                    currentTask->exec(currentTask);
                    currentTask->hasInterrupt = false;
                } else {
                    if (!currentTask->check_condition) {
                        if (!(currentTick % currentTask->timeToRun_ms)) {
                            currentTask->exec(currentTask);
                        }
                    } else {
                        if (!(currentTick % currentTask->timeToRun_ms)) {
                            if (currentTask->check_condition(currentTask)) {
                                currentTask->exec(currentTask);
                            }
                        }
                    }
                }
            }
        } else {
            if (currentTask->hasInterrupt) {
                currentTask->exec(currentTask);
                currentTask->hasInterrupt = false;
            } else {
                if (currentTask->check_condition) {
                    // handle not timed task with condition
                    if (currentTask->check_condition(currentTask)) {
                        currentTask->exec(currentTask);
                    }
                } else {
                    // handle not timed tasks without condition
                    currentTask->exec(currentTask);
                }
            }
        }
    }
}

/**
 * @brief getTaskData
 * @param idx
 * @return
 */
const sfsTask *getTaskData(uint8_t idx) {
    if (idx < TASK_TABLE_SIZE) {
        return TaskTable[idx];
    }
    return NULL;
}

/**
 * @brief suspendTask
 * @param idx
 */
void suspendTaskFlagCtrl(int val, uint8_t idx) {
    if (idx < TASK_TABLE_SIZE) {
        TaskTable[idx]->suspendTask = val;
    }
}

/**
 * @brief getTaskById
 * @param id
 * @return
 */
sfsTask *getTaskById(uint8_t id) {
    for (int i = 0; i < TASK_TABLE_SIZE; i++) {
        if (TaskTable[i]->id == id)
            return TaskTable[i];
    }

    return NULL;
}

const sfsTask *getNextTask() {
    int current_task_idx = 0;

    for (int i = 0; i < TASK_TABLE_SIZE; i++) {
        if (TaskTable[i] == currentTask) {
            current_task_idx = i + 1;
            break;
        }
    }

    while (1) {

        if (TaskTable[current_task_idx % TASK_TABLE_SIZE] != NULL) {
            if (TaskTable[current_task_idx % TASK_TABLE_SIZE]->suspendTask == false) {
                return TaskTable[current_task_idx % TASK_TABLE_SIZE];
            }
        }
        current_task_idx++;
    }
}

void inline taskSetResult(sfsTask *td, uint8_t res) { td->lastResult = res; }
