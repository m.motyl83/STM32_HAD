/**
*****************************************************************************
**
**  File        : syscalls.c
**
**  Abstract    : System Workbench Minimal System calls file
**
** 		          For more information about which c-functions
**                need which of these lowlevel functions
**                please consult the Newlib libc-manual
**
**  Environment : System Workbench for MCU
**
**  Distribution: The file is distributed as is, without any warranty
**                of any kind.
**
**  (c)Copyright System Workbench for MCU.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. System Workbench for MCU permit registered System Workbench(R)
*users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the System Workbench for MCU toolchain.
**
*****************************************************************************
*/

/* Includes */
#include "drivers/stm32f10x_it.h"
#include <errno.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/times.h>
#include <sys/unistd.h>
#include <time.h>

/**
 * Functions in this file are needed when we use original printf from c library
 * for exaple printf
 */

extern int errno;
extern void __io_putchar(char ch) __attribute__((weak));
extern char __io_getchar(void) __attribute__((weak));

register char *stack_ptr asm("sp");

int _getpid(void) { return 1; }

int _kill(int pid, int sig) {
  (void)pid;
  (void)sig;
  errno = EINVAL;
  return -1;
}

void _exit(int status) {
  _kill(status, -1);
  while (1) {
  } /* Make sure we hang here */
}

int _gettimeofday(struct timeval *tv, struct timezone *tz) {
  (void)tz;
  tv->tv_sec = getSystemTickMs() / 1000;
  tv->tv_usec = (getSystemTickMs() % 1000) * 1000;

  return 0;
}

int _read(int file, char *ptr, int len) {
  int DataIdx = 0;

  //  for (DataIdx = 0; DataIdx < len; DataIdx++) {
  //    *ptr++ = __io_getchar();
  //  }

  switch (file) {
  case STDIN_FILENO:
      while (1) {
          *ptr = __io_getchar();

          if (*ptr == '\n' || *ptr == '\r')
              return DataIdx;

          ptr++;
          DataIdx++;
      }
      break;

  default:
    return -EIO;
  }

  return len;
}

int _close(int file) {
  (void)file;
  return -1;
}

/*
 fstat
 Status of an open file. For consistency with other minimal implementations in
 these examples,
 all files are regarded as character special devices.
 The `sys/stat.h' header file required is distributed in the `include'
 subdirectory for this C library.
 */
int _fstat(int file, struct stat *st) {
  (void)file;
  st->st_mode = S_IFCHR;
  return 0;
}

/*
 isatty
 Query whether output stream is a terminal. For consistency with the other
 minimal implementations,
 */
int _isatty(int file) {
  switch (file) {
  case STDOUT_FILENO:
  case STDERR_FILENO:
  case STDIN_FILENO:
    return 1;
  default:
    // errno = ENOTTY;
    errno = EBADF;
    return 0;
  }
}

/*
 lseek
 Set position in a file. Minimal implementation:
 */
int _lseek(int file, int ptr, int dir) {
  (void)file;
  (void)ptr;
  (void)dir;
  return 0;
}

int _write(int file, char *ptr, int len) {
  int DataIdx;

  switch (file) {
  case STDOUT_FILENO:
  case STDERR_FILENO:
    for (DataIdx = 0; DataIdx < len; DataIdx++) {
      __io_putchar(*ptr++);
    }
    break;

  default:
    return -EIO;
  }

  return DataIdx;
}

caddr_t _sbrk(int incr) {
  extern char end asm("end");
  static char *heap_end;
  char *prev_heap_end;

  if (heap_end == 0)
    heap_end = &end;

  prev_heap_end = heap_end;
  if (heap_end + incr > stack_ptr) {
    // write(1, "Heap and stack collision\n", 25);
    errno = ENOMEM;
    return (caddr_t)-1;
  }

  heap_end += incr;

  return (caddr_t)prev_heap_end;
}
