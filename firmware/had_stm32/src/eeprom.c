#include "eeprom.h"
#include <stdint.h>
#include <stdlib.h>
#include <stm32f10x_flash.h>
#include <string.h>

/**
 * @brief eeprom_erase
 * @return EE_NO_ERROR when sucessfull
 */
EEPROM_STATUS eeprom_erase() {

  FLASH_Status res;

  __disable_irq();
  FLASH_Unlock();
  res = FLASH_ErasePage(EE_START);
  FLASH_Lock();
  __enable_irq();

  if (res == FLASH_COMPLETE)
    return EE_NO_ERROR;

  return EE_ERROR;
}

/**
 * @brief eeprom_read_block - read data block from flash
 * @param __dst
 * @param __src
 * @param __n
 * @return EE_NO_ERROR when sucessfull, else EE_RD_ERROR
 */
EEPROM_STATUS eeprom_read_block(char *__dst, char *__src, unsigned int __n) {

  uint32_t max = (uint32_t)(__src + __n);

  if (max > EE_END)
    return EE_ERROR;

  memcpy(__dst, __src, __n);
  return EE_NO_ERROR;
}

/**
 * @brief eeprom_write_block - write data block into flash
 * @param __dst
 * @param __src
 * @param __n
 * @return EE_NO_ERROR when sucessfull, else EE_WR_ERROR
 */
EEPROM_STATUS eeprom_write_block(const char *__dst, char *__src,
                                 unsigned int __n) {

  char *ee_tmp = malloc(EE_SIZE);

  if (ee_tmp == NULL) {
    return EE_ERROR;
  }

  const uint32_t offset = (uint32_t)(__dst - EE_START);

  if (offset > EE_SIZE) {
    free(ee_tmp);
    return EE_ERROR;
  }

  if ((ee_tmp + offset + __n) > (ee_tmp + EE_SIZE)) {
    free(ee_tmp);
    return EE_ERROR;
  }

  // read all eeprom content
  memcpy(ee_tmp, (char *)EE_START, EE_SIZE);

  // update ee_temp with new data
  memcpy((ee_tmp + (uint32_t)offset), __src, __n);

  __disable_irq();
  FLASH_Unlock();

  // erase whole eeprom flash page(1k)
  FLASH_ErasePage(EE_START);

  // copy new data into flash
  uint32_t tmp = 0;
  uint32_t data = 0;
  uint32_t *data_p = (uint32_t *)ee_tmp;
  uint32_t len = EE_SIZE;
  uint32_t err = 0;

  for (;;) {

    data = *data_p;
    FLASH_Status stat = FLASH_ProgramWord((EE_START + tmp), data);

    if (stat != FLASH_COMPLETE) {
      err = 1;
      break;
    }

    tmp += sizeof(uint32_t);
    len -= sizeof(uint32_t);
    data_p++;

    if (!len)
      break;
  }

  FLASH_Lock();
  __enable_irq();

  free(ee_tmp);

  if (err)
    return EE_ERROR;

  return EE_NO_ERROR;
}
