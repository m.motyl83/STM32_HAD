#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "debug.h"
#include "libeth/ethernet.h"
#include "hadRequests.h"
#include "output.h"
#include "temperatureReadings.h"

void HadTCPOpen(TCPSocket Socket, uint32_t IP)
{
    UNUSED(Socket);
    UNUSED(IP);
    DEBUG_MSG("HadTCPOpen\n");
    
}

void HadTCPClose(TCPSocket Socket) { UNUSED(Socket); }

/**
 * @brief netstackUDPIPProcess
 * @param packet
 */
void HadTCPCb(TCPSocket Socket, const uint8_t *Buffer, size_t Length)
{
    uint8_t cmd_magic;
    size_t payloadlen = 0;
    uint8_t *data2Send;

    if (false == tcp_start_packet(Socket, &data2Send, &payloadlen))
        return;

    if (Length > payloadlen)
        return;

    uint8_t *payloaddata = (uint8_t *)malloc(Length);

    if (payloaddata == NULL)
        return;

    memcpy(payloaddata, Buffer, Length);

    cmd_magic = payloaddata[0];

    if (cmd_magic == 0x55)
    {
        uint8_t cmd = payloaddata[1];

        payloaddata[0] = 0xAA;
        payloaddata[1] = cmd;

        switch (cmd)
        {
        case CMD_READ_CONFIG:
            memset(payloaddata + 2, 0, payloadlen - 2);
            memcpy(payloaddata + 2, getConfigData(), CONFIG_LEN);
            break;

        case CMD_SAVE_CONFIG:
            saveConfig();
            break;

        case CMD_SEND_CONFIG:
            memcpy((char *)getConfigData(), payloaddata + 2, CONFIG_LEN);
            break;

        case CMD_SET_FEATURE_VALUE:
            DEBUG_MSG("Set output value:%d %d\n", payloaddata[3], payloaddata[2]);
            setOutputStatusByFid(payloaddata[2], payloaddata[3]);
            break;

        case CMD_GET_FEATURE_VALUE:
        {
            // memset(payloaddata,0,payloadlen);
            uint8_t idx = payloaddata[2];
            uint8_t type = payloaddata[3];
            memset(payloaddata + 2, 0, payloadlen - 2);

            // we can use idx as node number in case temperature read
            if (type == TEMPERATURE)
            {
                uint16_t t = getNodeTemperatureByFid(idx);
                payloaddata[2] = (t & 0x00ff);
                payloaddata[3] = (t & 0xff00) >> 8;
            }
            else
                payloaddata[2] = getOutputStatusByFid((uint8_t)idx);
        }
        break;

        case CMD_GET_FEATURE:
        {
            uint8_t feature_idx = payloaddata[2];
            memset(payloaddata + 2, 0, payloadlen - 2);
            memcpy(payloaddata + 2, &features[feature_idx], sizeof(features));
        }
        break;

        case CMD_GET_FEATURE_NUMBER:
            memset(payloaddata + 2, 0, payloadlen - 2);
            payloaddata[2] = getConfigData()->feturesCnt;
            break;

        case CMD_HANDSHAKE:
            break;

        case CMD_SET_FEATURE_NAME:
        {
            // save feature name
            uint8_t idx = payloaddata[2];
            char *dst = (char *)features[idx].featureName;
            char *src = (char *)&payloaddata[3];

            if (idx < MAX_FEATURES)
            {
                memcpy(dst, src, FEATURE_NAME_LEN);
                saveFeatures();
            }
            memset(payloaddata + 2, 0, payloadlen - 2);
        }
        break;

        case CMD_SET_HOST_NAME:
        {
            memcpy((void *)getConfigData()->hostName, (char *)&payloaddata[2],
                   MAX_HOST_NAME_LEN);
            saveConfig();

            memset(payloaddata + 2, 0, payloadlen - 2);
        }
        break;

        case CMD_GET_HOST_NAME:
        {
            memset(payloaddata + 2, 0, payloadlen - 2);

            memcpy((char *)payloaddata + 2, (void *)getConfigData()->hostName,
                   MAX_HOST_NAME_LEN);
        }
        break;

        case CMD_DISCONNECT:
        {
            free(payloaddata);
            tcp_disconnect(Socket);
            return;
        }

        case CMD_GET_ALL_FEATURES:
        {

            // send all features
            // response: |0xaa|cmd|total num of features|host name 32bytes|f1|val
            // 2Bytes|f2|val 2Bytes|f3|val 2Bytes|

            uint16_t configLen = sizeof(features[0]);

            // if buffer length is not large enough
            if (payloadlen < (getConfigData()->feturesCnt * configLen))
            {
                free(payloaddata);
                return;
            }

            memset(payloaddata + 2, 0, payloadlen - 2);

            payloaddata[2] = getConfigData()->feturesCnt;
            memcpy(&payloaddata[3], getConfigData()->hostName, MAX_HOST_NAME_LEN);
            uint8_t *ptr = &payloaddata[(3 + MAX_HOST_NAME_LEN)];

            for (uint8_t i = 0; i < getConfigData()->feturesCnt; i++)
            {
                memcpy(ptr, &features[i], configLen);
                ptr += configLen;

                if (features[i].featureType == TEMPERATURE)
                {

                    uint16_t t = getNodeTemperatureByFid(features[i].featureId);
                    *ptr++ = (t & 0x00ff);
                    *ptr++ = (t & 0xff00) >> 8;
                }
                else
                {
                    *ptr++ = 0;
                    *ptr++ = getOutputStatusByFid(features[i].featureId);
                }
            }

            memcpy(data2Send, payloaddata, Length);
            tcp_send(Length);
            free(payloaddata);
        }
            return;

        default:
            free(payloaddata);
            return;
        }

        memcpy(data2Send, payloaddata, Length);
        tcp_send(Length);
        free(payloaddata);
    }
}

void HadUdpOpen(TCPSocket Socket, uint32_t IP)
{
    UNUSED(Socket);
    UNUSED(IP);
}

void HadUdpClose(TCPSocket Socket) { UNUSED(Socket); }

/**
 * @brief netstackUDPIPProcess
 * @param packet
 */
void HadUdpCb(TCPSocket Socket, const uint8_t *Buffer, size_t Length,
              bool isBroadcast)
{
    uint8_t cmd_magic;
    size_t payloadlen = 0;
    uint8_t *data2Send;

    if (false == udp_start_packet(Socket, &data2Send, &payloadlen))
        return;

    if (Length > payloadlen)
        return;

    uint8_t *payloaddata = (uint8_t *)malloc(Length);

    if (payloaddata == NULL)
        return;

    memcpy(payloaddata, Buffer, Length);

    cmd_magic = payloaddata[0];

    if (cmd_magic == 0x55)
    {
        uint8_t cmd = payloaddata[1];

        // if broadcast then only respond  when CMD_HANDSHAKE received
        if (isBroadcast == 1 && cmd != CMD_HANDSHAKE)
            return;

        payloaddata[0] = 0xAA;
        payloaddata[1] = cmd;

        switch (cmd)
        {

        case CMD_HANDSHAKE:
            break;

        default:
            free(payloaddata);
            return;
        }

        memcpy(data2Send, payloaddata, Length);
        udp_send(Length);
        free(payloaddata);
    }
}
