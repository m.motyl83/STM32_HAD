#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "debug.h"
#include "misc.h"
#include "network.h"
#include "state_machine.h"
#include "sysLed.h"
#include "temperatureReadings.h"
#include "output.h"
#include "button.h"
int main()
{
    DEBUG_MSG("Starting STM32_HAD....\n");

    LoadConfig();
    initButton();
    initUsDelayDwt();
    initUsDelay();
    initLedTask();
    initNetworkTask();
    initTemperatureReadTask();
    initOutputDevice();

    while (true)
    {
        handleTasks();

        // char f = __io_getchar();
        // if (f) {
        //   DEBUG_MSG("DDFD:%c\r\n", f);
        // }
    }

    return EXIT_SUCCESS;
}
