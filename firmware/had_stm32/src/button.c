#include "button.h"
#include "config.h"
#include "debug.h"
#include "state_machine.h"
#include "output.h"

static sfsTask buttonTask;
static uint8_t buttonsStatus = 0;

static input_node inputNodes[4] = {
    {
        .port = GPIOB,
        .pin = GPIO_Pin_4,
        .id = 0,
    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_5,
        .id = 1,
    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_6,
        .id = 2,

    },
    {
        .port = GPIOB,
        .pin = GPIO_Pin_7,
        .id = 3,
    }};

uint8_t getButtonsStatus() { return buttonsStatus; }

static void readButton(input_node *node)
{
    uint8_t pinValue = GPIO_ReadInputDataBit(node->port, node->pin);

    if (!pinValue && (buttonsStatus & (1 << node->id)) == 0)
    {
        if (!node->buttonDebunce--)
        {

            node->buttonDebunce = DEBOUNCE_TIME;
            buttonsStatus |= (1 << node->id);
            node->canUpdate = 1;
        }
    }
    else
    {
        if (buttonsStatus & (1 << node->id))
        {
            if (pinValue)
            {
                if (!node->buttonDebunce--)
                {
                    node->buttonDebunce = DEBOUNCE_TIME;
                    buttonsStatus &= ~(1 << node->id);
                    node->canUpdate = 1;
                }
            }
        }
        else
            node->buttonDebunce = DEBOUNCE_TIME;
    }
}

static void buttonFunction(sfsTask *tdata)
{

    for (size_t i = 0; i < 4; i++)
    {
        readButton(&inputNodes[i]);

        if (inputNodes[i].canUpdate)
        {
            inputNodes[i].canUpdate = 0;

            if ((buttonsStatus & (1 << inputNodes[i].id)) && inputNodes[i].bPressed == 0)
            {
                inputNodes[i].bPressed = 1;
                setOutputStatusByFid(inputNodes[i].id, true);
            }
            else
            {
                if ((buttonsStatus & (1 << inputNodes[i].id)) && inputNodes[i].bPressed == 1)
                {
                    inputNodes[i].bPressed = 0;
                    setOutputStatusByFid(inputNodes[i].id, false);
                }
            }
        }
    }

    taskSetResult(tdata, 0);
}

static bool buttonInit()
{
    // configure button pin as input
    GPIO_InitTypeDef GpioInitstructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    for (size_t i = 0; i < 4; i++)
    {
        GpioInitstructure.GPIO_Mode = GPIO_Mode_IPU;
        GpioInitstructure.GPIO_Pin = inputNodes[i].pin;
        GpioInitstructure.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_Init(inputNodes[i].port, &GpioInitstructure);
        inputNodes[i].buttonDebunce = DEBOUNCE_TIME;
        inputNodes[i].canUpdate = 0;
    }

    return true;
}

void initButton() { addTask(&buttonTask); }

static sfsTask buttonTask = {
    .id = 0xff,
    .timeToRun_ms = 10,
    .lastResult = 0,
    .hasInterrupt = false,
    .exec = buttonFunction,
    .init = buttonInit,
    .check_condition = NULL,
    .private_data = NULL,
    .exit = NULL,
    .suspendTask = false,
    .init_failed = false,
};
