#include "temperatureReadings.h"
#include "config.h"
#include "debug.h"
#include "drivers/stm32f10x_conf.h"
#include "drivers/stm32f10x_gpio.h"
#include "ds18b20.h"
#include "state_machine.h"
#include "stm32f10x_it.h"

static ds18b20Node temperatureNodes[1] = {
    {
        .dq_port = GPIOB,
        .dq_pin = GPIO_Pin_9,
        .temperature = 0,
        .enabled = 0,
        .conversionStatus = CONVERSION_INIT,
    }};

static sfsTask temeperatureTask;

/**
 * @brief temeperatureFunction
 * @param tdata
 */
static void temeperatureFunction(sfsTask *tdata)
{
    for (int i = 0; i < 1; i++)
    {
        if (!temperatureNodes[i].enabled || temperatureNodes[i].sensorOk == false)
            continue;
            
        ds18b20Gettemp(&temperatureNodes[i]);
    }
}

/**
 * @brief temeperatureInit
 * @return
 */
static bool temeperatureInit()
{
    uint8_t fCnt = getConfigData()->feturesCnt;

    for (int i = 0, j = 0; i < fCnt; i++)
    {

        if (getFeatureData()[i].featureType != TEMPERATURE)
            continue;

        temperatureNodes[j].enabled = true;
        temperatureNodes[j].featureId = getFeatureData()[i].featureId;

        if (false == ds18b20_setRes(&temperatureNodes[j], _10_BIT))
        {
            temperatureNodes[j].enabled = false;
            DEBUG_MSG("Failed to inialize Temperature node %d\n", j);
        }

        j++;
    }
    return true;
}

/**
 * @brief addTemeperatureTask
 */
void initTemperatureReadTask() { addTask(&temeperatureTask); }

/**
 * @brief getNodeTemperature
 * @param node_num
 * @return
 */
uint16_t getNodeTemperature(uint8_t nodeNum)
{

    if (nodeNum >= T_MAX_NODES)
        return 0xffff;

    if (temperatureNodes[nodeNum].enabled == false)
        return 0xffff;

    return temperatureNodes[nodeNum].temperature;
}

/**
 * @brief getNodeTemperatureByFid
 * @param featureId
 * @return
 */
uint16_t getNodeTemperatureByFid(uint8_t featureId)
{

    for (uint8_t i = 0; i < T_MAX_NODES; i++)
    {
        if (temperatureNodes[i].featureId == featureId &&
            temperatureNodes[i].enabled)
            return temperatureNodes[i].temperature;
    }
    return 0xffff;
}

static sfsTask temeperatureTask = {
    .id = 0xff,
    .timeToRun_ms = 1000,
    .lastResult = 0,
    .hasInterrupt = false,
    .exec = temeperatureFunction,
    .init = temeperatureInit,
    .check_condition = NULL,
    .private_data = NULL,
    .exit = NULL,
    .suspendTask = false,
    .init_failed = false,
};
