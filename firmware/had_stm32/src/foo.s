.syntax	unified
.arch	armv7-m

.text
.align 2

.equ CYCCNT2, 0xE0001004

.text
.global foo
#.extern cnt1
.type	foo, %function

foo:
    push {r7, lr}

    add r7, sp, #0
    sub sp, sp, 8

    ldr r0, =CYCCNT2
    str r0, [r7]

#    ldr r0, =cnt1
#    mov r1, #0xAA
#    str r1, [r0]
    mov sp, r7
    sub sp, sp, 0

    pop {r7, pc}
