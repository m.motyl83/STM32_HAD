/*! \file ds18b20.c \brief Temperature reading. */

/*
Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/
#include <stdbool.h>
#include <stdio.h>


#include "drivers/stm32f10x_conf.h"
#include "drivers/stm32f10x_gpio.h"
#include "stm32f10x_it.h"
#include "debug.h"
#include "ds18b20.h"
#include "misc.h"

uint8_t ds18b20_reset_asm(ds18b20Node *node);
/**
 * @brief ds18b20_reset_asm
 * @return
 */
uint8_t ds18b20_reset(ds18b20Node *node) {
  uint8_t i;

  // low for 480us
  GPIO_ResetBits(node->dq_port, node->dq_pin);
  delayUs(480);

  // release line and wait for 60uS
  GPIO_SetBits(node->dq_port, node->dq_pin);
  delayUs(60);

  // get value and wait 420us
  i = GPIO_ReadInputDataBit(node->dq_port, node->dq_pin);
  delayUs(420);
  // return the read value, 0=ok, 1=error
  return i;
}

/**
 * @brief ds18b20_writebit
 * @param bit
 */
void ds18b20_writebit(ds18b20Node *node, uint8_t bit) {

  GPIO_ResetBits(node->dq_port, node->dq_pin);
  delayUs(1);

  if (bit)
    GPIO_SetBits(node->dq_port, node->dq_pin);

  delayUs(60);
  GPIO_SetBits(node->dq_port, node->dq_pin);
}

/**
 * @brief ds18b20_readbit
 * @return
 */
uint8_t ds18b20_readbit(ds18b20Node *node) {
  uint8_t bit = 0;

  // low for 1uS
  GPIO_ResetBits(node->dq_port, node->dq_pin);
  delayUs(1);
  GPIO_SetBits(node->dq_port, node->dq_pin);

  delayUs(14);
  // release line and wait for 14uS

  // read the value
  if (GPIO_ReadInputDataBit(node->dq_port, node->dq_pin))
    bit = 1;

  // wait 45uS and return read value
  delayUs(45);
  return bit;
}

/**
 * @brief ds18b20_writebyte
 * @param byte
 */
void ds18b20Writebyte(ds18b20Node *node, uint8_t byte) {
  uint8_t i = 8;
  __asm("cpsid i");
  while (i--) {
    ds18b20_writebit(node, (byte & 1));
    byte >>= 1;
  }
  __asm("cpsie i");
}

/**
 * @brief ds18b20_readbyte
 * @return
 */
uint8_t ds18b20Readbyte(ds18b20Node *node) {
  uint8_t i = 8, n = 0;
  __asm("cpsid i");
  while (i--) {
    n >>= 1;
    n |= (ds18b20_readbit(node) << 7);
  }
  __asm("cpsie i");
  return n;
}

/**
 * @brief setRes
 * @param res
 */
bool ds18b20_setRes(ds18b20Node *node, ds18b20res res) {
  uint8_t dsData[5];
  uint8_t i;

  __asm("cpsid i");

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
  GPIO_InitStructure.GPIO_Pin = node->dq_pin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(node->dq_port, &GPIO_InitStructure);

#ifdef DS18B20_STOPINTERRUPTONREAD
  __disable_irq();
#endif

  if (ds18b20_reset_asm(node)) // reset
  {
    node->sensorOk = false;
    __asm("cpsie i");
    return false;
  }

  node->sensorOk = true;

  ds18b20Writebyte(node, DS18B20_CMD_SKIPROM);     // skip ROM 70
  ds18b20Writebyte(node, DS18B20_CMD_RSCRATCHPAD); // read scratchpad 70

  for (i = 0; i < 5; i++)
    dsData[i] = ds18b20Readbyte(node);

  if (res == _9_BIT)
    dsData[4] = 0x1f;
  if (res == _10_BIT)
    dsData[4] = 0x3f;
  if (res == _11_BIT)
    dsData[4] = 0x5f;
  if (res == _12_BIT)
    dsData[4] = 0x7f;

  ds18b20_reset_asm(node); // reset

  ds18b20Writebyte(node, DS18B20_CMD_SKIPROM);     // skip ROM 70
  ds18b20Writebyte(node, DS18B20_CMD_WSCRATCHPAD); // read scratchpad 70

  for (i = 2; i < 5; i++)
    ds18b20Writebyte(node, dsData[i]);

#ifdef DS18B20_STOPINTERRUPTONREAD
  __enable_irq();
#endif

  __asm("cpsie i");
  return true;
}

/**
 * @brief ds18b20_gettemp
 * @return
 */
char ds18b20Gettemp(ds18b20Node *node) {
  char res = 0;

#ifdef DS18B20_STOPINTERRUPTONREAD
  __disable_irq();
#endif

  switch (node->conversionStatus) {
  case CONVERSION_INIT: {
    if (ds18b20_reset_asm(
            node)) { // reset 960 - if we don't recevie response from
                     // slave we just exit
      node->sensorOk = false;
      res = CONVERSION_ERROR;
      return res;
    }
    ds18b20Writebyte(node, DS18B20_CMD_SKIPROM); // skip ROM 70
    ds18b20Writebyte(
        node,
        DS18B20_CMD_CONVERTTEMP); // start temperature conversion 70
    node->conversionStatus = CONVERSION_IN_PROGRESS;
  } break;

  case CONVERSION_IN_PROGRESS:
    if (ds18b20_readbit(node)) {
      node->conversionStatus = CONVERSION_INIT;
      ds18b20_reset_asm(node);                          // reset
      ds18b20Writebyte(node, DS18B20_CMD_SKIPROM);     // skip ROM 70
      ds18b20Writebyte(node, DS18B20_CMD_RSCRATCHPAD); // read scratchpad 70

      uint8_t tempL = ds18b20Readbyte(node);
      uint8_t tempH = ds18b20Readbyte(node);

      uint16_t temp;
      // double retd;
      temp = (uint16_t)((tempH << 8) | tempL);
      // retd = temp * 0.0625;
        DEBUG_MSG("***************\n");
      node->temperature = temp;
      res = CONVERSION_END;

    } else
      res = CONVERSION_IN_PROGRESS;
    break;

  default:
    break;
  }

#ifdef DS18B20_STOPINTERRUPTONREAD
  __enable_irq();
#endif

  return res;
}
