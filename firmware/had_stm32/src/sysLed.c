#include "config.h"
#include "debug.h"
#include "state_machine.h"

static sfsTask ledTask;

/**
 * @brief SysLedInit
 * @return
 */
static bool sysLedInit() {
  GPIO_InitTypeDef GpioInitstructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

  GpioInitstructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GpioInitstructure.GPIO_Pin = SYSTEM_LED_PIN;
  GpioInitstructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(SYSTEM_LED_PORT, &GpioInitstructure);
  GPIO_SetBits(SYSTEM_LED_PORT, SYSTEM_LED_PIN);

  return SUCCESS;
}

static void ledFunction(sfsTask *tdata) {
  UNUSED(tdata);
  GPIO_WriteBit(SYSTEM_LED_PORT, SYSTEM_LED_PIN,
                !GPIO_ReadOutputDataBit(SYSTEM_LED_PORT, SYSTEM_LED_PIN));
}

void initLedTask() { addTask(&ledTask); }

static sfsTask ledTask = {
    .id = 0xff,
    .timeToRun_ms = 250,
    .lastResult = 0,
    .hasInterrupt = false,
    .exec = ledFunction,
    .init = sysLedInit,
    .check_condition = NULL,
    .private_data = NULL,
    .exit = NULL,
    .suspendTask = false,
    .init_failed = false,
};
