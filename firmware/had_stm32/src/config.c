/*! \file config.c \brief Loading system config from EEPROM. */

#include <stdio.h>
#include <string.h>

#include "config.h"
#include "eeprom.h"
#include "libeth/ethernet.h"

/** \brief sys_config config - contains loaded device configuration from
 * EEPROM*/
struct sysConfig config;

/** \brief dev_features features - contains loaded device features from EEPROM
 */
struct devFeatures features[MAX_FEATURES];

static struct sysConfig ee_config EEMEM = {
    .dhcp = STATIC_IP,
    .staticIp = MAKE_IP(192, 168, 1, 25),
    .gateway = MAKE_IP(192, 168, 1, 1),
    .netmask = MAKE_IP(255, 255, 255, 0),
    .hostName = "HAD_G_STM32",
    .mac = {0x10, 0x00, 0x00, 0xf9, 0xf3, 0x17},
    .feturesCnt = MAX_FEATURES,
    .extPort = 9900,
};

static struct devFeatures dev_features_ee[MAX_FEATURES] EEMEM = {
    {.featureType = REALY_OUTPUT, .featureId = 0, .featureName = "OUTPUT_0"},
    {.featureType = REALY_OUTPUT, .featureId = 1, .featureName = "OUTPUT_1"},
    {.featureType = REALY_OUTPUT, .featureId = 2, .featureName = "OUTPUT_2"},
    {.featureType = REALY_OUTPUT, .featureId = 3, .featureName = "OUTPUT_3"},

#ifdef ENABLE_DS18B20
    {.featureType = TEMPERATURE, .featureId = 4, .featureName = "TEMP_1"},
#endif
};

/**
 * @brief LoadConfig
 */
void LoadConfig()
{
    memset((char *)&features, 0xff, FEATURES_LEN);

    eeprom_read_block((char *)&config, (char *)&ee_config, CONFIG_LEN);
    eeprom_read_block((char *)&features, (char *)&dev_features_ee, FEATURES_LEN);
}

/**
 * @brief saveConfig
 */
void saveConfig()
{
    eeprom_write_block((char *)&ee_config, (char *)&config, CONFIG_LEN);
}

/**
 * @brief saveFeatures
 */
void saveFeatures()
{
    eeprom_write_block((char *)&dev_features_ee, (char *)&features, FEATURES_LEN);
}

/**
 * @brief getConfigData
 * @return
 */
struct sysConfig *getConfigData()
{
    return &config;
}

/**
 * @brief getFeatureData
 * @return
 */
struct devFeatures *getFeatureData()
{
    return features;
}
