#include "global_defines.h"
#include "printf.h"

#define UNUSED(x) (void)(x)

#define STRINGIFY(s) XSTRINGIFY(s)
#define XSTRINGIFY(s) #s

#ifdef ENABLE_UART_DEBUG
/*
#pragma message("Setting SERIAL_PORT_BAUDRATE is " STRINGIFY(                  \
    UART_DEBUG_SPEED) "bps")
*/

#define DEBUG_MSG(fmt, args...)                                                \
  tfp_printf("[DEBUG]:" fmt, ##args) /*info function */
#else
#define DEBUG_MSG(fmt, args...)
#endif

void USARTdebugInit(void);
void tfp_puts(const char *str);
char __io_getchar(void);
