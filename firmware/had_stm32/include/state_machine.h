#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**

    SFS stands for "simple function scheduler"

*/

#define TASK_TIMEOUT_MS 2000
#define TASK_TABLE_SIZE 16
#define TASK_MIN_TIME_MS 1

#define C_PRIVATE(T) struct T##private {
#define C_PRIVATE_END                                                          \
  }                                                                            \
private                                                                        \
  ;

#define C_PRIV(x) ((x).private)
#define C_PRIV_REF(x) (&(x)->private)

typedef struct __sfs sfsTask;

// TaskFunction CAN'T use while loops and delays !!! Should be pass through!!!!
typedef void (*onExecute)(sfsTask *thisTask);
typedef int (*onCondition)(sfsTask *thisTask);
typedef bool (*onInit)(void);
typedef void (*onExit)(void);

struct __sfs {
  uint8_t
      lastResult; // last result of task function - used with ConditionFunction
  uint8_t id;
  bool suspendTask;
  bool init_failed;
  bool hasInterrupt;
  uint32_t timeToRun_ms; // time interval in ms - not mandatory

  onExecute exec; // pointer to function to execute/invoke - mandatory
  onCondition check_condition; // pointer to function which is invoked before
                               // exec - not mandatory
  const onInit init; // function to invoke before add to global tasks list - not
                     // mandatory
  const onExit
      exit; // function to invoke when remove taks from global tasks list -
            // not mandatory
  void *private_data; // pointer to other structures which points to other
                      // functions provided by "Task"
  uint32_t timeCorrection;
} __attribute__((packed));

int addTask(sfsTask *td);
int removeTask(sfsTask *td);
void handleTasks();
const sfsTask *getTaskData(uint8_t idx);
sfsTask *getTaskById(uint8_t id);
void suspendTaskFlagCtrl(int val, uint8_t idx);
void taskSetResult(sfsTask *td, uint8_t res);
void signalINT(int taskID);
#ifdef __cplusplus
}
#endif
