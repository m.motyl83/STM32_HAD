#include "drivers/stm32f10x_conf.h"
#include "drivers/stm32f10x.h"
#include "global_defines.h"

#define SYSTICK_HZ          1000 // HZ
#define SYSTEM_LED_PORT     (GPIOC)
#define SYSTEM_LED_PIN      (GPIO_Pin_13)
#define USER_LED_PORT       (GPIOC)
#define USER_LED_PIN        (GPIO_Pin_14)
#define CONFIG_LEN          sizeof(config)
#define FEATURES_LEN        sizeof(features)
#define FEATURE_NAME_LEN    32
#define MAX_HOST_NAME_LEN   32

#ifdef ENABLE_DS18B20
#define MAX_FEATURES        5
#else
#define MAX_FEATURES        2
#endif

#define STATIC_IP           0
#define USE_DHCP            1
#define REALY_OUTPUT        0
#define TEMPERATURE         1
#define DEFAULT_PORT        8888

#define ENC28J68_RESET_PORT GPIOA
#define ENC28J68_RESET_PIN  GPIO_Pin_2

#define ENC28J68_CS_PORT GPIOA
#define ENC28J68_CS_PIN  GPIO_Pin_3

#define SPI1_PORT       GPIOA
#define SPI1_SCK_PIN    GPIO_Pin_5
#define SPI1_MISO_PIN   GPIO_Pin_6
#define SPI1_MOSI_PIN   GPIO_Pin_7

/*
#if defined HAD_V1
#pragma message("Selected HAD_V1 version")
#else
#if defined HAD_V2_PCB
#pragma message("Selected HAD_V2_PCB version")
#else
#error "HAD version not defined !!! Please define HAD_V1 or HAD_V2_PCB"
#endif
#endif
*/

struct sysConfig {
  uint8_t       dhcp;                        // dhcp flag - if 1 ,use dhcp,else static
  uint32_t      staticIp;                 // static ip
  uint32_t      gateway;                  // static ip
  uint32_t      netmask;                  // static ip
  uint8_t       hostName[MAX_HOST_NAME_LEN]; // host name when dhcp used
  uint8_t       mac[6];                      // mac addr
  uint8_t       feturesCnt;
  uint16_t      extPort;
} __attribute__((packed));

struct devFeatures {
  uint8_t featureType; // 0 - relay output,1 -temperature input
  uint8_t featureId;
  uint8_t featureName[FEATURE_NAME_LEN];
};

extern struct sysConfig config;
extern struct devFeatures features[MAX_FEATURES];

void LoadConfig(void);
void saveConfig(void);
void saveFeatures(void);
struct sysConfig *getConfigData(void);
struct devFeatures *getFeatureData(void);
