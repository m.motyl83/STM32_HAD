#include <stdint.h>
#include <stdlib.h>

// Example usage
// static char tab[] EEMEM = {"some data"};

#define EEMEM __attribute__((aligned(1), section(".eeprom")))

#define EE_START    (0x0800fc00)
#define EE_END      (0x0800fc00 + 1024)
#define EE_SIZE     (EE_END - EE_START)

typedef enum {
  EE_ERROR = 1,
  EE_NO_ERROR,
} EEPROM_STATUS;

EEPROM_STATUS eeprom_read_block(char *__dst, char *__src, unsigned int __n);
EEPROM_STATUS eeprom_write_block(const char *__dst, char *__src, unsigned int __n);
