#include <stdbool.h>
#include <stdint.h>

#include "libeth/ethernet.h"

#define CMD_SET_FEATURE_VALUE 0x01
#define CMD_GET_FEATURE_VALUE 0x02
#define CMD_SEND_CONFIG 0x03
#define CMD_SAVE_CONFIG 0x04
#define CMD_READ_CONFIG 0x05
#define CMD_GET_FEATURE 0x06
#define CMD_GET_FEATURE_NUMBER 0x07
#define CMD_HANDSHAKE 0x08
#define CMD_SET_FEATURE_NAME 0x09
#define CMD_SET_HOST_NAME 0x0A
#define CMD_GET_HOST_NAME 0x0B
#define CMD_GET_ALL_FEATURES 0x0C
#define CMD_REBOOT 0x0D
#define CMD_DISCONNECT 0x0E

void HadTCPOpen(TCPSocket Socket, uint32_t IP);
void HadTCPClose(TCPSocket Socket);
void HadTCPCb(TCPSocket Socket, const uint8_t *Buffer, size_t Length);

void HadUdpOpen(UDPSocket Socket, uint32_t IP);
void HadUdpClose(UDPSocket Socket);
void HadUdpCb(UDPSocket Socket, const uint8_t *Buffer, size_t Length,
              bool isBroadcast);