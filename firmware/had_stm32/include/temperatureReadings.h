#include "ds18b20.h"
#include <stdint.h>

enum tNodes { T_OUT, T_STOVE, T_WATER, T_FEEDER, T_MAX_NODES };

void initTemperatureReadTask();
uint16_t getNodeTemperature(uint8_t nodeNum);
uint16_t getNodeTemperatureByFid(uint8_t featureId);
