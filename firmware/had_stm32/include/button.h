#include <stdint.h>
#include "drivers/stm32f10x_gpio.h"

#define BUTTON1 0x01
#define BUTTON1_HOLD 0x10

#define BUTTON2 0x02
#define BUTTON2_HOLD 0x20

#define BUTTON3 0x04
#define BUTTON3_HOLD 0x40

#define BUTTON4 0x08
#define BUTTON4_HOLD 0x80

#define DEBOUNCE_TIME 20

void initButton();

uint8_t getButtonsStatus();


typedef struct _input_node input_node;

struct _input_node {
  GPIO_TypeDef *port;
  uint16_t pin;
  uint8_t id;
  uint8_t buttonDebunce;
  uint8_t bPressed;
  uint8_t canUpdate;

} __attribute__((packed));
