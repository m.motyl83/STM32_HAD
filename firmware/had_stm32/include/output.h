/*! \file output.h \brief Output configuration and manipulation. */

#include <stdbool.h>
#include <stdio.h>
#include "drivers/stm32f10x_gpio.h"

typedef enum {
  OUTPUT_0,
  OUTPUT_1,
  OUTPUT_2,
  OUTPUT_3,
  OUTPUT_MAX_NODES
} outputNodesNames;

typedef struct _output_node output_node;

struct _output_node {
  GPIO_TypeDef *port;
  uint16_t pin;
  uint8_t enabled;
  uint8_t featureId;

} __attribute__((packed));



void initOutputDevice(void);

void setOutputStatus(outputNodesNames outId, FunctionalState new_state);
void setOutputStatusByFid(uint8_t featureId, FunctionalState new_state);
uint8_t getOutputStatus(outputNodesNames outId);
uint8_t getOutputStatusByFid(uint8_t featureId);



