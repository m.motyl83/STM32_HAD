/*! \file ds18b20.h \brief Temperature reading. */

#ifndef DS18B20_H_
#define DS18B20_H_

#include "drivers/stm32f10x_gpio.h"
#include <stdbool.h>

typedef struct __ds18b20_node ds18b20Node;

struct __ds18b20_node {
  GPIO_TypeDef *dq_port;
  uint16_t dq_pin;
  uint16_t temperature;
  uint8_t conversionStatus;
  uint8_t enabled;
  uint8_t sensorOk;
  uint8_t featureId;
} __attribute__((packed));

// commands
#define DS18B20_CMD_CONVERTTEMP 0x44
#define DS18B20_CMD_RSCRATCHPAD 0xbe
#define DS18B20_CMD_WSCRATCHPAD 0x4e
#define DS18B20_CMD_CPYSCRATCHPAD 0x48
#define DS18B20_CMD_RECEEPROM 0xb8
#define DS18B20_CMD_RPWRSUPPLY 0xb4
#define DS18B20_CMD_SEARCHROM 0xf0
#define DS18B20_CMD_READROM 0x33
#define DS18B20_CMD_MATCHROM 0x55
#define DS18B20_CMD_SKIPROM 0xcc
#define DS18B20_CMD_ALARMSEARCH 0xec

// stop any interrupt on read
#define DS18B20_DISABLE_INT_ON_READ 1

#define CONVERSION_INIT 0
#define CONVERSION_IN_PROGRESS 1
#define CONVERSION_ERROR 2
#define CONVERSION_END 3

typedef enum { _9_BIT, _10_BIT, _11_BIT, _12_BIT } ds18b20res;

extern char ds18b20Gettemp(ds18b20Node *node);
bool ds18b20_setRes(ds18b20Node *node, ds18b20res res);

#endif
